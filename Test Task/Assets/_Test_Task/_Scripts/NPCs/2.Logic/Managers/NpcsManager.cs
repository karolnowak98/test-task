using System;
using System.Collections.Generic;
using System.Linq;
using TestTask.Core.Logic;
using TestTask.Npcs.Data;
using TestTask.Routines.Logic;
using TestTask.Teleports.Logic;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;
using IInitializable = Zenject.IInitializable;
using Object = UnityEngine.Object;

namespace TestTask.Npcs.Logic
{
    public interface INpcsManager : IInitializable, IDisposable, IFixedTickable
    {
        public void TeleportNpc(int p_instanceID);
    }
    
    public class NpcsManager : INpcsManager
    {
        private readonly NpcsConfigs _configs;
        private readonly List<Npc> _npcs = new List<Npc>();
        private readonly IGameManager _gameManager;
        private readonly IGlobalNpcsManager _globalNpcsManager;
        private readonly ITeleportsManager _teleportsManager;
        private readonly IRoutinesManager _routinesManager;
        private readonly NpcController.Factory _npcFactory;
        private readonly GameObject _parentNpcsGameObject;
        private readonly int _sceneIndex;
        
        public NpcsManager(NpcsConfigs p_configs, GameObject p_npcsGameObject, IGameManager p_gameManager, 
            ITeleportsManager p_teleportsManager, IRoutinesManager p_routinesManager, IGlobalNpcsManager p_globalNpcsManager,
            NpcController.Factory p_npcFactory)
        {
            _configs = p_configs;
            _gameManager = p_gameManager;
            _teleportsManager = p_teleportsManager;
            _routinesManager = p_routinesManager;
            _globalNpcsManager = p_globalNpcsManager;
            _parentNpcsGameObject = p_npcsGameObject;
            _npcFactory = p_npcFactory;
            _sceneIndex = _parentNpcsGameObject.scene.buildIndex;
            
            InitNpcs(_parentNpcsGameObject);
        }
        
        public void Initialize()
        {
            _gameManager.OnMinuteTick += HandleNpcsTasks;
            _gameManager.OnSeasonChange += _ => CheckForNewSchedule();
            _gameManager.OnWeatherChange += _ => CheckForNewSchedule();
            _globalNpcsManager.OnNpcTeleport += CreateNpc;
        }
        
        public void Dispose()
        {
            _gameManager.OnMinuteTick -= HandleNpcsTasks;
            _gameManager.OnSeasonChange -= _ => CheckForNewSchedule();
            _gameManager.OnWeatherChange -= _ => CheckForNewSchedule();
            _globalNpcsManager.OnNpcTeleport -= CreateNpc;
        }
        
        private void CreateNpc(int p_sceneIndex, Npc p_npc)
        {
            if (p_sceneIndex != _sceneIndex)
            {
                return;
            }
            
            var npcGameObject = _npcFactory.Create().gameObject;
            
            npcGameObject.transform.parent = _parentNpcsGameObject.transform;
            npcGameObject.transform.position = _teleportsManager.GetTeleportPosition(p_sceneIndex, p_npc.CurrentSceneIndex).position;
            npcGameObject.transform.rotation = p_npc.Transform.rotation;
            
            var newNpc = (Npc) p_npc.Clone();
            
            newNpc.CurrentSceneIndex = p_sceneIndex;
            newNpc.Transform = npcGameObject.transform;
            newNpc.ID = npcGameObject.transform.GetInstanceID();
            
            _npcs.Add(newNpc);
        }
        
        public void FixedTick()
        {
            foreach (var npc in _npcs.Where(p_npc => p_npc.IsMoving))
            {
                if (Math.Abs(npc.Transform.position.magnitude - npc.CurrentTarget.position.magnitude) < 0.01f)
                {
                    npc.IsMoving = false;
                    npc.CurrentTask = npc.CurrentDayTask.TaskName;
                    npc.Transform.rotation = Quaternion.Euler(npc.CurrentDayTask.TaskDirection == TaskDirection.Left 
                        ? new Vector3(0f, -180f, 0f) : new Vector3(0f, 0f, 0f));
                    
                    if (npc.IsMovingToTeleport)
                    {
                        npc.IsMovingToTeleport = false;
                    }
                    
                    continue;
                }
                
                npc.Transform.position = Vector3.MoveTowards(npc.Transform.position, npc.CurrentTarget.position, npc.Config.MoveSpeed);
            }
        }

        public void TeleportNpc(int p_instanceID)
        {
            var npc = _npcs.Find(p_npc => p_npc.ID == p_instanceID);

            if (!(npc is { IsMovingToTeleport: true }))
            {
                return;
            }

            RemoveNpc(npc);
        }

        private void RemoveNpc(Npc p_npc)
        {
            p_npc.IsMovingToTeleport = false;
            p_npc.IsMoving = false;
            _globalNpcsManager.TeleportNpc(p_npc);
            _npcs.Remove(p_npc);
            Object.Destroy(p_npc.Transform.gameObject);
        }

        private void CheckForNewSchedule()
        {
            foreach(var npc in _npcs)
            {
                var newDaySchedule = GetAvailableSchedule(npc);

                if (newDaySchedule == null)
                {
                    continue;
                }
                
                npc.CurrentDaySchedule = newDaySchedule.Value;
            }
        }
        
        private void HandleNpcsTasks()
        {
            foreach (var npc in _npcs)
            {
                foreach (var dayTask in npc.CurrentDaySchedule.DayTasks)
                {
                    if (!AreTaskConditionsMet(dayTask))
                    {
                        continue;
                    }
                    
                    npc.CurrentDayTask = dayTask;
                }
            }

            foreach (var npc in _npcs.Where(p_npc => !p_npc.IsCurrentlyExecutingTask))
            {
                if (!npc.IsOnRightScene)
                {
                    if (!npc.IsMoving)
                    {
                        MoveToTeleport(npc);
                    }
                }
                
                else
                {
                    if (!npc.IsMoving)
                    {
                        MoveToRoutineTarget(npc);
                    }
                }
            }
        }

        private void MoveToRoutineTarget(Npc p_npc)
        {
            var routine = _routinesManager.GetClosestRoutineTarget(p_npc.CurrentSceneIndex, p_npc.CurrentDayTask.TaskName);

            if (routine == null)
            {
                return;
            }
            
            p_npc.CurrentTarget = routine;
            p_npc.IsMoving = true;
        }

        private void MoveToTeleport(Npc p_npc)
        {
            var teleport = _teleportsManager.GetTeleportPosition(p_npc.CurrentSceneIndex, p_npc.CurrentDayTask.SceneIndex);
            
            if (teleport == null)
            {
                return;
            }

            p_npc.CurrentTarget = teleport;
            p_npc.IsMovingToTeleport = true;
            p_npc.IsMoving = true;
        }
        
        private DaySchedule? GetAvailableSchedule(Npc p_npc)
        {
            foreach (var daySchedule in p_npc.Config.DaySchedules)
            {
                if (daySchedule.SchedulePriority < p_npc.CurrentDaySchedule.SchedulePriority)
                {
                    if (AreScheduleConditionsMet(daySchedule))
                    {
                        return daySchedule;
                    }
                }
            }

            return null;
        }

        private bool AreScheduleConditionsMet(DaySchedule p_schedule)
        {
            if ((p_schedule.DoesRequireSeason) && (_gameManager.GameState.Season != p_schedule.RequiredSeason))
            {
                return false;
            }

            if ((p_schedule.DoesRequireWeather) && (_gameManager.GameState.Weather != p_schedule.RequiredWeather))
            {
                return false;
            }
            
            return true;
        }

        private bool AreTaskConditionsMet(DayTask p_task)
        {
            if (p_task.GameTimeCondition.Hours != _gameManager.GameState.GameTime.Hours)
            {
                return false;
            }
            
            if (p_task.GameTimeCondition.Minutes != _gameManager.GameState.GameTime.Minutes)
            {
                return false;
            }

            if (p_task.GameTimeCondition.DayCycle != _gameManager.GameState.GameTime.DayCycle)
            {
                return false;
            }
            
            return true;
        }
        
        private void InitNpcs(GameObject p_npcs)
        {
            for (var i = 0; i < p_npcs.transform.childCount; i++)
            {
                var config = _configs.GetConfigByName(NpcName.Harmony);
                
                if (config == null)
                {
                    continue;
                }

                var tempNpc = new Npc(config.Value, p_npcs.transform.GetChild(i).transform, _sceneIndex);
                _npcs.Add(tempNpc);
            }
        }
    }
}