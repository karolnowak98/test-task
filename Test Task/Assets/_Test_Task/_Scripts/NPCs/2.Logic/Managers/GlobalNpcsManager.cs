﻿using System;
using TestTask.Npcs.Data;

namespace TestTask.Teleports.Logic
{
    public interface IGlobalNpcsManager
    {
        public void TeleportNpc(Npc p_npc);
        public event Action<int, Npc> OnNpcTeleport; //Key scene index
    }
    
    public class GlobalNpcsManager : IGlobalNpcsManager
    {
        public event Action<int, Npc> OnNpcTeleport;
        
        public void TeleportNpc(Npc p_npc)
        {
            OnNpcTeleport?.Invoke(p_npc.CurrentDayTask.SceneIndex, p_npc);
        }
    }
}