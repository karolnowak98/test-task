using TestTask.Npcs.Logic;
using UnityEngine;
using Zenject;

namespace TestTask.Npcs
{
    [RequireComponent(typeof(Animator))]
    public class NpcController : MonoBehaviour
    {
        private INpcsManager _npcsManager;
        private Animator _animator;

        [Inject]
        public void Construct(INpcsManager p_npcsManager)
        {
            _npcsManager = p_npcsManager;
        }
        
        //TODO control animation in fixed
        
        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void OnTriggerEnter2D(Collider2D p_collider)
        {
            if (p_collider.CompareTag("Teleport"))
            {
                _npcsManager.TeleportNpc(transform.GetInstanceID());
            }
        }
        
        public class Factory : PlaceholderFactory<NpcController> { }
    }
}