﻿using Zenject;

namespace TestTask.Teleports.Logic
{
    public class GlobalNpcsInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IGlobalNpcsManager>().To<GlobalNpcsManager>().AsSingle();
        }
    }
}