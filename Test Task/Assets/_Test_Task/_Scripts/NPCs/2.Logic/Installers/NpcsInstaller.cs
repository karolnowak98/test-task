using System;
using TestTask.Npcs.Data;
using UnityEngine;
using Zenject;

namespace TestTask.Npcs.Logic
{
    public class NpcsInstaller : MonoInstaller
    {
        [SerializeField] private NpcsConfigs _configs;
        [SerializeField] private GameObject _npcs;
    
        public override void InstallBindings()
        {
            Container.Bind<INpcsManager>().To<NpcsManager>().AsSingle().WithArguments(_configs, _npcs);
            Container.Bind<IInitializable>().To<INpcsManager>().FromResolve();
            Container.Bind<IDisposable>().To<INpcsManager>().FromResolve();
            Container.Bind<IFixedTickable>().To<INpcsManager>().FromResolve();
            
            Container.BindFactory<NpcController, NpcController.Factory>().FromComponentInNewPrefab(_configs.GetConfigByName(NpcName.Harmony)?.Prefab);
        }
    }
}