﻿using System;
using TestTask.Core.Data;
using UnityEngine;

namespace TestTask.Npcs.Data
{
    public enum NpcName { Harmony, Other }
    public enum TaskName { None, Work, Sleep, Eat }
    public enum TaskDirection { Left, Right}
    
    [CreateAssetMenu(fileName = "NPCs Configs", menuName = "Configs/NPCs Configs")]
    public class NpcsConfigs : ScriptableObject
    {
        [SerializeField] private NpcConfig[] _npcSchedules;

        public NpcConfig? GetConfigByName(NpcName p_name)
        {
            foreach (var config in _npcSchedules)
            {
                if (config.NpcName != p_name)
                {
                    continue;
                }
                
                return config;
            }
            
            return null;
        }
    }

    [Serializable]
    public struct NpcConfig
    {
        [SerializeField] private NpcName _npcName;
        [SerializeField] private GameObject _prefab;
        [SerializeField, Range(0, 10)] private float _moveSpeed;
        [SerializeField] private DaySchedule[] _daySchedules;

        public GameObject Prefab => _prefab;
        public NpcName NpcName => _npcName;
        public float MoveSpeed => _moveSpeed;
        public DaySchedule[] DaySchedules => _daySchedules;
    }

    [Serializable]
    public struct DaySchedule
    {
        [SerializeField, Min(0)] private int _schedulePriority; //The lower number, the higher priority
        [SerializeField] private bool _doesRequireWeather;
        [SerializeField] private bool _doesRequireSeason; 
        [SerializeField] private Weather _requiredWeather; //If I would have Odin, I would put here ShowIf
        [SerializeField] private Season _requiredSeason; //If I would have Odin, I would put here ShowIf
        [SerializeField] private DayTask[] _dayTasks;

        public int SchedulePriority => _schedulePriority;
        public bool DoesRequireWeather => _doesRequireWeather;
        public bool DoesRequireSeason => _doesRequireSeason;
        public Weather RequiredWeather => _requiredWeather;
        public Season RequiredSeason => _requiredSeason;
        public DayTask[] DayTasks => _dayTasks;
    }

    [Serializable]
    public struct DayTask
    {
        [SerializeField] private GameTime _gameTimeCondition;
        [SerializeField] private int _sceneIndex;
        [SerializeField] private TaskName _taskName;
        [SerializeField] private TaskDirection _taskDirection;

        public GameTime GameTimeCondition => _gameTimeCondition;
        public int SceneIndex => _sceneIndex;
        public TaskName TaskName => _taskName;
        public TaskDirection TaskDirection => _taskDirection;
    }
}
