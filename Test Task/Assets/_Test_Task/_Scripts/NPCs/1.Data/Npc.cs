﻿using System;
using System.Linq;
using ModestTree;
using UnityEngine;

namespace TestTask.Npcs.Data
{
    public class Npc : ICloneable
    {
        internal int ID { get; set; }
        internal int CurrentSceneIndex { get; set; }
        internal bool IsMovingToTeleport { get; set; }
        internal bool IsMoving { get; set; }
        internal bool IsOnRightScene => CurrentSceneIndex == CurrentDayTask.SceneIndex;
        internal bool IsCurrentlyExecutingTask => CurrentTask == CurrentDayTask.TaskName;
        internal Transform CurrentTarget { get; set; }
        internal Transform Transform { get; set; }
        internal NpcConfig Config { get; }
        internal DaySchedule CurrentDaySchedule { get; set; }
        internal DayTask CurrentDayTask { get; set; }
        internal TaskName CurrentTask { get; set; }

        public Npc(NpcConfig p_config, Transform p_transform, int p_currentSceneIndex)
        {
            Transform = p_transform;
            ID = p_transform.GetInstanceID();
            Config = p_config;
            CurrentSceneIndex = p_currentSceneIndex;
            CurrentTask = TaskName.None;
            CurrentDaySchedule = Config.DaySchedules.Last();
            
            if (CurrentDaySchedule.DayTasks.IsEmpty())
            {
                Debug.LogError("There is no any day task in config!");
            }
            
            else
            {
                CurrentDayTask = CurrentDaySchedule.DayTasks.First();
            }
        }
        
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}