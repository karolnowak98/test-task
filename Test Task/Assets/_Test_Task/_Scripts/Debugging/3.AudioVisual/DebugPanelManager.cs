﻿using System;
using ModestTree;
using TestTask.Cameras.Logic;
using TestTask.Core.Data;
using TestTask.Core.Logic;
using TestTask.Input.Logic;
using TMPro;
using UnityEngine;
using Zenject;

namespace TestTask.Debugging.Logic
{
    public interface IDebugPanelManager : IInitializable, IDisposable
    {
        
    }
    
    public class DebugPanelManager : IDebugPanelManager
    {
        private readonly IInputManager _inputManager;
        private readonly IGameManager _gameManager;
        private readonly ICamerasManager _camerasManager;
        private readonly DebugPanelReferences _references;
        private bool IsPanelVisible => _references.gameObject.activeInHierarchy;

        public DebugPanelManager(IInputManager p_inputManager, IGameManager p_gameManager, 
            ICamerasManager p_camerasManager, DebugPanelReferences p_references)
        {
            _inputManager = p_inputManager;
            _gameManager = p_gameManager;
            _camerasManager = p_camerasManager;
            _references = p_references;

            _references.DebugPanel.HoursInput.characterValidation = TMP_InputField.CharacterValidation.Integer;
            _references.DebugPanel.MinutesInput.characterValidation = TMP_InputField.CharacterValidation.Integer;
            _references.DebugPanel.DayInput.characterValidation = TMP_InputField.CharacterValidation.Integer;
        }
        
        public void Initialize()
        {
            _references.DebugPanel.ApplyButton.onClick.AddListener(ApplyConfig);
            
            _references.DebugPanel.ChangeToScene1Button.onClick.AddListener(delegate
            {
                _camerasManager.ChangeScene(1);
            });
            
            _references.DebugPanel.ChangeToScene2Button.onClick.AddListener(delegate
            {
                _camerasManager.ChangeScene(2);
            });
            
            _references.DebugPanel.ChangeToScene3Button.onClick.AddListener(delegate
            {
                _camerasManager.ChangeScene(3);
            });
            
            _inputManager.OnTildeButtonClick += ShowHideDebugPanel;
            _gameManager.OnDebugPanelVisible += () => IsPanelVisible;
        }
        
        public void Dispose()
        {
            _references.DebugPanel.ApplyButton.onClick.RemoveAllListeners();
            _references.DebugPanel.ChangeToScene1Button.onClick.RemoveAllListeners();
            _references.DebugPanel.ChangeToScene2Button.onClick.RemoveAllListeners();
            _references.DebugPanel.ChangeToScene3Button.onClick.RemoveAllListeners();
            
            _inputManager.OnTildeButtonClick -= ShowHideDebugPanel;
        }

        private void ApplyConfig()
        {
            if (!ValidateFields())
            {
                return;
            }

            var gameTime = new GameTime(int.Parse(_references.DebugPanel.HoursInput.text),
                int.Parse(_references.DebugPanel.MinutesInput.text), (DayCycle) _references.DebugPanel.DayCycle.value);
            
            var newGameState = new GameState()
            {
                Weather = (Weather) _references.DebugPanel.WeatherDropdown.value,
                Day = int.Parse(_references.DebugPanel.DayInput.text),
                Season = (Season) _references.DebugPanel.SeasonDropdown.value,
                GameTime = gameTime
            };
            
            _gameManager.ChangeGameState(newGameState);
        }

        private void ShowHideDebugPanel()
        {
            if (_references.isActiveAndEnabled)
            {
                _references.gameObject.SetActive(false);
                return;
            }
            
            _references.gameObject.SetActive(true);
        }

        private bool ValidateFields()
        {
            if (_references.DebugPanel.HoursInput.text.IsEmpty())
            {
                Debug.LogWarning("Hours shouldn't be empty!");
                return false;
            }
            
            if (_references.DebugPanel.MinutesInput.text.IsEmpty())
            {
                Debug.LogWarning("Minutes shouldn't be empty!");
                return false;
            }
            
            if (_references.DebugPanel.DayInput.text.IsEmpty())
            {
                Debug.LogWarning("Day shouldn't be empty!");
                return false;
            }
            
            var hours = int.Parse(_references.DebugPanel.HoursInput.text);
            var minutes = int.Parse(_references.DebugPanel.MinutesInput.text);
            var day = int.Parse(_references.DebugPanel.DayInput.text);
            
            if(hours < 0 || hours > 24)
            {
                Debug.LogWarning("Hours should be in 0-24 range!");
                return false;
            }
            
            if(minutes < 0 || minutes > 60)
            {
                Debug.LogWarning("Minutes should be in 0-60 range!");
                return false;
            }

            if (day >= 1 && day <= 28)
            {
                return true;
            }
            
            Debug.LogWarning("Day should be in 1-28 range!");
            return false;
        }
    }
}