﻿using UnityEngine;
using Zenject;

namespace TestTask.Debugging.Logic
{
    public class DebugAudioVisualInstaller : MonoInstaller
    {
        [SerializeField] private DebugPanelReferences _debugReferences;
            
        public override void InstallBindings()
        {
            Container.Bind<DebugPanelManager>().AsSingle().WithArguments(_debugReferences);
            Container.Bind<IInitializable>().To<DebugPanelManager>().FromResolve();
        }
    }
}