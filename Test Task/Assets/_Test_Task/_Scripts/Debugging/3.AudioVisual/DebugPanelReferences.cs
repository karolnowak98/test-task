﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TestTask.Debugging.Logic
{
    public class DebugPanelReferences : MonoBehaviour
    {
        [SerializeField] private DebugPanel _debugPanel;

        public DebugPanel DebugPanel => _debugPanel;
    }

    [Serializable]
    public struct DebugPanel
    {
        [SerializeField] private TMP_Dropdown _weatherDropdown;
        [SerializeField] private TMP_InputField _hoursInput;
        [SerializeField] private TMP_InputField _minutesInput;
        [SerializeField] private TMP_Dropdown _dayCycleDropdown;
        [SerializeField] private TMP_InputField _dayInput;
        [SerializeField] private TMP_Dropdown _seasonDropdown;
        [SerializeField] private Button _applyButton;
        [SerializeField] private Button _changeToScene1Button;
        [SerializeField] private Button _changeToScene2Button;
        [SerializeField] private Button _changeToScene3Button;

        public TMP_Dropdown WeatherDropdown => _weatherDropdown;
        public TMP_InputField HoursInput => _hoursInput;
        public TMP_InputField MinutesInput => _minutesInput;
        public TMP_Dropdown DayCycle => _dayCycleDropdown;
        public TMP_InputField DayInput => _dayInput;
        public TMP_Dropdown SeasonDropdown => _seasonDropdown;
        public Button ApplyButton => _applyButton;
        public Button ChangeToScene1Button => _changeToScene1Button;
        public Button ChangeToScene2Button => _changeToScene2Button;
        public Button ChangeToScene3Button => _changeToScene3Button;
    }
}