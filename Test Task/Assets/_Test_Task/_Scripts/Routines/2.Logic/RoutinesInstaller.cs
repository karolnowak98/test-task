﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TestTask.Routines.Logic
{
    public class RoutinesInstaller : MonoInstaller
    {
        [SerializeField] private List<RoutineConfig> _configs = new List<RoutineConfig>();

        public override void InstallBindings()
        {
            Container.Bind<IRoutinesManager>().To<RoutinesManager>().AsSingle().WithArguments(_configs);
        }
    }
}