﻿using System.Collections.Generic;
using System.Linq;
using TestTask.Npcs.Data;
using UnityEngine;

namespace TestTask.Routines.Logic
{
    public interface IRoutinesManager
    {
        public Transform GetClosestRoutineTarget(int p_sceneIndex, TaskName p_taskName);
    }
    
    public class RoutinesManager : IRoutinesManager
    {
        private readonly List<Routine> _routines = new List<Routine>();
        
        public RoutinesManager(List<RoutineConfig> p_configs)
        {
            foreach (var config in p_configs)
            {
                _routines.Add(new Routine(config));
            }
        }

        public Transform GetClosestRoutineTarget(int p_sceneIndex, TaskName p_taskName)
        {
            return (from routine in _routines 
                where routine.RoutineConfig.SceneIndex == p_sceneIndex 
                where routine.RoutineConfig.TaskName == p_taskName 
                select routine.RoutineConfig.Transform).FirstOrDefault();
        }
    }
}
