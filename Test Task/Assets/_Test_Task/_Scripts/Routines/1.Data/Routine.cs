﻿namespace TestTask.Routines.Logic
{
    public class Routine
    {
        public RoutineConfig RoutineConfig { get; set; }

        public Routine(RoutineConfig p_config)
        {
            RoutineConfig = p_config;
        }
    }
}
