﻿using System;
using TestTask.Npcs.Data;
using UnityEngine;

namespace TestTask.Routines.Logic
{
    [Serializable]
    public class RoutineConfig
    {
        [SerializeField] private TaskName _taskName;
        [SerializeField] private int _sceneIndex;
        [SerializeField] private Transform _targetTransform;

        public TaskName TaskName => _taskName;
        public int SceneIndex => _sceneIndex;
        public Transform Transform => _targetTransform;
    }
}