using System.Collections.Generic;
using System.Linq;
using TestTask.Teleports.Data;
using UnityEngine;

namespace TestTask.Teleports.Logic
{
    public interface ITeleportsManager
    {
        public Transform GetTeleportPosition(int p_currentScene, int p_teleportToScene);
    }
    
    public class TeleportsManager : ITeleportsManager
    {
        private readonly List<Teleport> _teleports = new List<Teleport>();
        private ITeleportsManager _teleportsManagerImplementation;

        public TeleportsManager(List<TeleportConfig> p_teleportConfigs)
        {
            foreach (var config in p_teleportConfigs)
            {
                _teleports.Add(new Teleport(config));
            }
        }

        public Transform GetTeleportPosition(int p_currentScene, int p_teleportToScene)
        {
            return (from teleport in _teleports 
                where teleport.TeleportConfig.TeleportSceneIndex == p_teleportToScene 
                where teleport.TeleportConfig.SceneIndex == p_currentScene 
                select teleport.TeleportConfig.Transform).FirstOrDefault();

            //TODO calculate path through global teleports controller
        }
    }
}