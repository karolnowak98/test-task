﻿using System.Collections.Generic;
using TestTask.Teleports.Data;
using UnityEngine;
using Zenject;

namespace TestTask.Teleports.Logic
{
    public class TeleportsInstaller : MonoInstaller
    {
        [SerializeField] private List<TeleportConfig> _configs = new List<TeleportConfig>();

        public override void InstallBindings()
        {
            Container.Bind<ITeleportsManager>().To<TeleportsManager>().AsSingle().WithArguments(_configs);
        }
    }
}