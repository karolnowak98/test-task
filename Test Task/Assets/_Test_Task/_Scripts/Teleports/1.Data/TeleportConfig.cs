﻿using System;
using UnityEngine;

namespace TestTask.Teleports.Data
{
    [Serializable]
    public class TeleportConfig
    {
        [SerializeField] private int _sceneIndex;
        [SerializeField] private int _teleportSceneIndex;
        [SerializeField] private Transform _transform;

        public int SceneIndex => _sceneIndex;
        public int TeleportSceneIndex => _teleportSceneIndex;
        public Transform Transform => _transform;
    }
}