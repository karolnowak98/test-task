﻿namespace TestTask.Teleports.Data
{
    public class Teleport
    {
        internal TeleportConfig TeleportConfig { get; }

        public Teleport(TeleportConfig p_config)
        {
            TeleportConfig = p_config;
        }
    }
}