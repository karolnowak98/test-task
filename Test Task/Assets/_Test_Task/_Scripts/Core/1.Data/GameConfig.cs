﻿using System;
using UnityEngine;

namespace TestTask.Core.Data
{
    [CreateAssetMenu(fileName = "Game Config", menuName = "Configs/Game Config")]
    [Serializable]
    public class GameConfig : ScriptableObject
    {
        [SerializeField, Min(1), Tooltip("How many real seconds to one hour in game.")] private int _realSecondsToOneGameHour;
        [SerializeField, Min(1), Tooltip("How many days for season.")] private int _daysInSeason;
        [SerializeField, Min(1), Tooltip("Time multiplier, when hold T")] private int _timeMultiplier;

        [Space(15), Header("Start settings")]
        [Space(5)]
        
        [SerializeField] private Season _startSeason;
        [SerializeField] private Weather _startWeather;
        [SerializeField, Range(1, 28)] private int _startDay;
        [SerializeField] private GameTime _gameTime;

        public int RealSecondsToOneGameHour => _realSecondsToOneGameHour;
        public int DaysInSeason => _daysInSeason;
        public int TimeMultiplier => _timeMultiplier;
        
        public Season StartSeason => _startSeason;
        public Weather StartWeather => _startWeather;
        public int StartDay => _startDay;
        public GameTime GameTime => _gameTime;
    }
}