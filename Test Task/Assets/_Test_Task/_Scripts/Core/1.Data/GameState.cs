﻿using System;
using UnityEngine;

namespace TestTask.Core.Data
{
    public enum Season { Winter, Spring, Summer, Autumn }
    public enum Weather { Sunny, Rainy, Snowing }
    public enum DayCycle { Am, Pm }

    [Serializable]
    public class GameState
    {
        [SerializeField] private Season _season;
        [SerializeField] private Weather _weather;
        [SerializeField] private GameTime _gameTime;
        [SerializeField] private int _day;
        
        public Season Season { get => _season; set => _season = value; }
        public Weather Weather { get => _weather; set => _weather = value; }
        public GameTime GameTime { get => _gameTime; set => _gameTime = value; }
        public int Day { get => _day; set => _day = value; }
    }

    [Serializable]
    public class GameTime
    {
        [SerializeField, Range(1, 12)] private int _hours;
        [SerializeField, Range(0, 59)] private int _minutes;
        [SerializeField] private DayCycle _dayCycle;

        public GameTime(int p_hours, int p_minutes, DayCycle p_dayCycle)
        {
            _hours = p_hours;
            _minutes = p_minutes;
            _dayCycle = p_dayCycle;
        }
        
        public int Hours { get => _hours; set => _hours = value; }
        public int Minutes { get => _minutes; set => _minutes = value; }
        public DayCycle DayCycle { get => _dayCycle; set => _dayCycle = value; }
    }
}