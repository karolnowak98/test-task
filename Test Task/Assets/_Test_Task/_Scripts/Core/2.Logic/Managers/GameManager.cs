﻿using System;
using System.Collections;
using TestTask.Core.Data;
using TestTask.Input.Logic;
using UnityEngine;
using Zenject;

namespace TestTask.Core.Logic
{
    public interface IGameManager : IInitializable, IDisposable
    {
        public GameState GameState { get; }

        public event Action OnMinuteTick;
        public event Action<int, int, DayCycle> OnTimeChange; // Hour > Minutes > IsAM
        public event Action<int> OnDayChange; //Day
        public event Action<Season> OnSeasonChange;
        public event Action<Weather> OnWeatherChange;
        public event Action<GameState> OnGameStateChange;
        public event GameManager.IsDebugPanelShown OnDebugPanelVisible;

        public void ChangeGameState(GameState p_gameState);
    }

    public class GameManager : IGameManager
    {
        private readonly GameConfig _gameConfig;
        private readonly IInputManager _inputManager;
        private readonly MonoBehaviour _monoBehaviour;
        private float _oneMinuteTime;

        public GameState GameState { get; private set; }

        public delegate bool IsDebugPanelShown();

        public event IsDebugPanelShown OnDebugPanelVisible;
        public event Action OnMinuteTick;
        public event Action<int, int, DayCycle> OnTimeChange;
        public event Action<int> OnDayChange;
        public event Action<Season> OnSeasonChange;
        public event Action<Weather> OnWeatherChange;
        public event Action<GameState> OnGameStateChange;

        public GameManager(GameConfig p_gameConfig, IInputManager p_inputManager, MonoBehaviour p_monoBehaviour)
        {
            _gameConfig = p_gameConfig;
            _inputManager = p_inputManager;
            _monoBehaviour = p_monoBehaviour;

            GameState = new GameState
            {
                Season = _gameConfig.StartSeason,
                Weather = _gameConfig.StartWeather,
                Day = _gameConfig.StartDay,
                GameTime = _gameConfig.GameTime,
            };
        }
        
        public void Initialize()
        {
            _oneMinuteTime = _gameConfig.RealSecondsToOneGameHour / 60f;
            _monoBehaviour.StartCoroutine(InitTimer());

            _inputManager.OnTButtonClick += SpeedUpTime;
            _inputManager.OnTButtonCanceled += SlowDownTime;
        }

        public void Dispose()
        {
            _inputManager.OnTButtonClick -= SpeedUpTime;
            _inputManager.OnTButtonCanceled -= SlowDownTime;
        }

        private void SpeedUpTime()
        {
            if (OnDebugPanelVisible == null || !OnDebugPanelVisible.Invoke())
            {
                _oneMinuteTime = (_gameConfig.RealSecondsToOneGameHour / 60f);
                return;
            }

            _oneMinuteTime = (_gameConfig.RealSecondsToOneGameHour / 60f) / _gameConfig.TimeMultiplier;
        }

        private void SlowDownTime()
        {
            _oneMinuteTime = (_gameConfig.RealSecondsToOneGameHour / 60f);
        }

        private IEnumerator InitTimer()
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(_oneMinuteTime);
                HandleGameMinuteTick();
            }
        }

        private void HandleGameMinuteTick()
        {
            GameState.GameTime.Minutes++;

            if (GameState.GameTime.Minutes == 60)
            {
                GameState.GameTime.Minutes = 0;
                GameState.GameTime.Hours++;
            }

            if (GameState.GameTime.Hours == 13)
            {
                GameState.GameTime.Hours = 1;

                if (GameState.GameTime.DayCycle == DayCycle.Am)
                {
                    GameState.GameTime.DayCycle = DayCycle.Pm;
                }

                else
                {
                    GameState.Day++;
                    GameState.GameTime.DayCycle = DayCycle.Am;
                    OnDayChange?.Invoke(GameState.Day);
                }
            }

            if (GameState.Day == _gameConfig.DaysInSeason + 1)
            {
                if ((int)GameState.Season == 3)
                {
                    GameState.Season = 0;
                }

                else
                {
                    GameState.Season++;
                }

                GameState.Day = 1;
                OnSeasonChange?.Invoke(GameState.Season);
            }

            OnTimeChange?.Invoke(GameState.GameTime.Hours, GameState.GameTime.Minutes, GameState.GameTime.DayCycle);
            OnMinuteTick?.Invoke();
        }

        public void ChangeGameState(GameState p_gameState)
        {
            GameState = p_gameState;
            OnGameStateChange?.Invoke(GameState);
            OnWeatherChange?.Invoke(GameState.Weather);
            OnSeasonChange?.Invoke(GameState.Season);
        }
    }
}