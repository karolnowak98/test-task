﻿using System;
using TestTask.Core.Data;
using UnityEngine;
using Zenject;

namespace TestTask.Core.Logic
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private GameConfig _gameConfig;

        public override void InstallBindings()
        {
            Container.Bind<IGameManager>().To<GameManager>().AsSingle().WithArguments(_gameConfig, this).NonLazy();
            Container.Bind<IDisposable>().To<IGameManager>().FromResolve();
            Container.Bind<IInitializable>().To<IGameManager>().FromResolve();
        }
    }
}
