﻿using System;
using UnityEngine;
using Zenject;

namespace TestTask.Core.AudioVisual
{
    public class GameAudioVisualInstaller : MonoInstaller
    {
        [SerializeField] private ClockPanelReferences _references;
        
        public override void InstallBindings()
        {
            Container.Bind<ClockPanelReferences>().ToSelf().AsSingle();
            Container.Bind<ITimePanelManager>().To<ClockPanelManager>().AsSingle().WithArguments(_references);
            Container.Bind<IDisposable>().To<ITimePanelManager>().FromResolve();
        }
    }
}