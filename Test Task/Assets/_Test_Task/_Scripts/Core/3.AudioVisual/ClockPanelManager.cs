﻿using System;
using TestTask.Core.Data;
using TestTask.Core.Logic;

namespace TestTask.Core.AudioVisual
{
    public interface ITimePanelManager : IDisposable
    {
        
    }
    
    public class ClockPanelManager : ITimePanelManager
    {
        private readonly ClockPanelReferences _references;
        private readonly IGameManager _gameManager;

        public ClockPanelManager(ClockPanelReferences p_references, IGameManager p_gameManager)
        {
            _references = p_references;
            _gameManager = p_gameManager;
            
            UpdatePanel(_gameManager.GameState);

            _gameManager.OnTimeChange += UpdateTime;
            _gameManager.OnDayChange += UpdateDay;
            _gameManager.OnSeasonChange += UpdateSeason;
            _gameManager.OnGameStateChange += UpdatePanel;
        }
        
        public void Dispose()
        {
            _gameManager.OnTimeChange -= UpdateTime;
            _gameManager.OnDayChange -= UpdateDay;
            _gameManager.OnSeasonChange -= UpdateSeason;
            _gameManager.OnGameStateChange -= UpdatePanel;
        }

        private void UpdateDay(int p_dayNumber)
        {
            _references.ClockPanel.Day.text = GetDayOfWeek(p_dayNumber) + " " + p_dayNumber;
        }

        private void UpdateTime(int p_hours, int p_minutes, DayCycle p_dayCycle)
        {
            _references.ClockPanel.Time.text = p_hours + ":";
            
            if (p_minutes < 10)
            {
                _references.ClockPanel.Time.text += "0";
            }

            _references.ClockPanel.Time.text += p_minutes;
            
            if (p_dayCycle == DayCycle.Am)
            {
                _references.ClockPanel.Time.text += " AM";
            }
            
            else
            {
                _references.ClockPanel.Time.text += " PM";
            }
        }

        private void UpdateSeason(Season p_newSeason)
        {
            _references.ClockPanel.SeasonImage.sprite = p_newSeason switch
            {
                Season.Summer => _references.ClockPanel.SummerSprite,
                Season.Autumn => _references.ClockPanel.AutumnSprite,
                Season.Winter => _references.ClockPanel.WinterSprite,
                Season.Spring => _references.ClockPanel.SpringSprite,
                _ => _references.ClockPanel.SeasonImage.sprite
            };
        }

        private void UpdateWeather(Weather p_newWeather)
        {
            _references.ClockPanel.WeatherImage.sprite = p_newWeather switch
            {
                Weather.Rainy => _references.ClockPanel.RainySprite,
                Weather.Sunny => _references.ClockPanel.SunnySprite,
                Weather.Snowing => _references.ClockPanel.SnowingSprite,
                _ => _references.ClockPanel.WeatherImage.sprite
            };
        }
        
        private void UpdatePanel(GameState p_gameState)
        {
            UpdateDay(p_gameState.Day);
            UpdateTime(p_gameState.GameTime.Hours, p_gameState.GameTime.Minutes, p_gameState.GameTime.DayCycle);
            UpdateWeather(p_gameState.Weather);
            UpdateSeason(p_gameState.Season);
        }

        private static string GetDayOfWeek(int p_dayNumber)
        {
            return (p_dayNumber % 7) switch
            {
                6 => DayOfWeek.Tuesday.ToString(),
                5 => DayOfWeek.Wednesday.ToString(),
                4 => DayOfWeek.Thursday.ToString(),
                3 => DayOfWeek.Friday.ToString(),
                2 => DayOfWeek.Saturday.ToString(),
                0 => DayOfWeek.Sunday.ToString(),
                _ => DayOfWeek.Monday.ToString()
            };
        }
    }
}