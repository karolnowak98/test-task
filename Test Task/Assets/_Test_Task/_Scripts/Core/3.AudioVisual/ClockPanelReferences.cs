﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TestTask.Core.AudioVisual
{
    public class ClockPanelReferences : MonoBehaviour
    {
        [SerializeField] private ClockPanel _clockPanel;

        public ClockPanel ClockPanel => _clockPanel;
    }

    [Serializable]
    public struct ClockPanel
    {
        [SerializeField] private TMP_Text _time;
        [SerializeField] private TMP_Text _day;
        
        [SerializeField] private Image _weatherImage;
        [SerializeField] private Sprite _weatherSunnySprite;
        [SerializeField] private Sprite _weatherRainySprite;
        [SerializeField] private Sprite _weatherSnowingSprite;
        
        [SerializeField] private Image _seasonImage;
        [SerializeField] private Sprite _seasonSummerSprite;
        [SerializeField] private Sprite _seasonAutumnSprite;
        [SerializeField] private Sprite _seasonWinterSprite;
        [SerializeField] private Sprite _seasonSpringSprite;

        public TMP_Text Time => _time;
        public TMP_Text Day => _day;
        
        public Image WeatherImage => _weatherImage;
        public Sprite SunnySprite => _weatherSunnySprite;
        public Sprite RainySprite => _weatherRainySprite;
        public Sprite SnowingSprite => _weatherSnowingSprite;
        
        public Image SeasonImage => _seasonImage;
        public Sprite SummerSprite => _seasonSummerSprite;
        public Sprite AutumnSprite => _seasonAutumnSprite;
        public Sprite WinterSprite => _seasonWinterSprite;
        public Sprite SpringSprite => _seasonSpringSprite;
    }
}