// GENERATED AUTOMATICALLY FROM 'Assets/_Test_Task/Input/GameControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GameControls : IInputActionCollection, IDisposable
{
    public InputActionAsset Asset { get; }
    public @GameControls()
    {
        Asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameControls"",
    ""maps"": [
        {
            ""name"": ""Game"",
            ""id"": ""533dd288-6eec-40de-b131-77cbd20e3ca5"",
            ""actions"": [
                {
                    ""name"": ""DebugPanel"",
                    ""type"": ""Button"",
                    ""id"": ""88a7d7c1-c9e9-4326-80ac-c70a9ac4d67d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""IncreaseTime"",
                    ""type"": ""Button"",
                    ""id"": ""c238fcde-8edd-4cae-87cd-79e6530d32d0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""e4c3785d-373c-418d-89f0-bdc2f662ce24"",
                    ""path"": ""<Keyboard>/backquote"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DebugPanel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0950de3b-a4d5-4ebe-b5db-054a36c2c655"",
                    ""path"": ""<Keyboard>/t"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""IncreaseTime"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Game
        _mGame = Asset.FindActionMap("Game", throwIfNotFound: true);
        _mGameDebugPanel = _mGame.FindAction("DebugPanel", throwIfNotFound: true);
        _mGameIncreaseTime = _mGame.FindAction("IncreaseTime", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(Asset);
    }

    public InputBinding? bindingMask
    {
        get => Asset.bindingMask;
        set => Asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => Asset.devices;
        set => Asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => Asset.controlSchemes;

    public bool Contains(InputAction p_action)
    {
        return Asset.Contains(p_action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return Asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        Asset.Enable();
    }

    public void Disable()
    {
        Asset.Disable();
    }

    // Game
    private readonly InputActionMap _mGame;
    private IGameActions _mGameActionsCallbackInterface;
    private readonly InputAction _mGameDebugPanel;
    private readonly InputAction _mGameIncreaseTime;
    public struct GameActions
    {
        private @GameControls _mWrapper;
        public GameActions(@GameControls p_wrapper) { _mWrapper = p_wrapper; }
        public InputAction @DebugPanel => _mWrapper._mGameDebugPanel;
        public InputAction @IncreaseTime => _mWrapper._mGameIncreaseTime;
        public InputActionMap Get() { return _mWrapper._mGame; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool Enabled => Get().enabled;
        public static implicit operator InputActionMap(GameActions p_set) { return p_set.Get(); }
        public void SetCallbacks(IGameActions p_instance)
        {
            if (_mWrapper._mGameActionsCallbackInterface != null)
            {
                @DebugPanel.started -= _mWrapper._mGameActionsCallbackInterface.OnDebugPanel;
                @DebugPanel.performed -= _mWrapper._mGameActionsCallbackInterface.OnDebugPanel;
                @DebugPanel.canceled -= _mWrapper._mGameActionsCallbackInterface.OnDebugPanel;
                @IncreaseTime.started -= _mWrapper._mGameActionsCallbackInterface.OnIncreaseTime;
                @IncreaseTime.performed -= _mWrapper._mGameActionsCallbackInterface.OnIncreaseTime;
                @IncreaseTime.canceled -= _mWrapper._mGameActionsCallbackInterface.OnIncreaseTime;
            }
            _mWrapper._mGameActionsCallbackInterface = p_instance;
            if (p_instance != null)
            {
                @DebugPanel.started += p_instance.OnDebugPanel;
                @DebugPanel.performed += p_instance.OnDebugPanel;
                @DebugPanel.canceled += p_instance.OnDebugPanel;
                @IncreaseTime.started += p_instance.OnIncreaseTime;
                @IncreaseTime.performed += p_instance.OnIncreaseTime;
                @IncreaseTime.canceled += p_instance.OnIncreaseTime;
            }
        }
    }
    public GameActions @Game => new GameActions(this);
    public interface IGameActions
    {
        void OnDebugPanel(InputAction.CallbackContext p_context);
        void OnIncreaseTime(InputAction.CallbackContext p_context);
    }
}
