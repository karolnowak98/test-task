using System;
using Zenject;

namespace TestTask.Input.Logic
{
    public interface IInputManager : IInitializable, IDisposable
    {
        public event Action OnTildeButtonClick;
        public event Action OnTButtonClick;
        public event Action OnTButtonCanceled;
    }
    
    public class InputManager : IInputManager
    {
        private readonly GameControls _gameControls;

        public event Action OnTildeButtonClick;
        public event Action OnTButtonClick;
        public event Action OnTButtonCanceled;
    
        public InputManager()
        {
            _gameControls = new GameControls();

            _gameControls.Game.DebugPanel.performed += p_ => OnTildeButtonClick?.Invoke();
            _gameControls.Game.IncreaseTime.performed += p_ => OnTButtonClick?.Invoke();
            _gameControls.Game.IncreaseTime.canceled += p_ => OnTButtonCanceled?.Invoke();
        }
    
        public void Initialize()
        {
            _gameControls.Enable();
        }
    
        public void Dispose()
        {
            _gameControls.Disable();
        }
    }
}