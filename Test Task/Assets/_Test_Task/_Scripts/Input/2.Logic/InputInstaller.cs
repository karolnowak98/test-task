﻿using Zenject;

namespace TestTask.Input.Logic
{
    public class InputInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IInputManager>().To<InputManager>().AsSingle().NonLazy();
            Container.Bind<IInitializable>().To<IInputManager>().FromResolve();
        }
    }
}