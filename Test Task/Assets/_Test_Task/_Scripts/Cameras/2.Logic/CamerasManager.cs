﻿using TestTask.Cameras.Data;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace TestTask.Cameras.Logic
{
    public interface ICamerasManager : IInitializable
    {
        public void ChangeScene(int p_sceneIndex);
    }
    
    public class CamerasManager : ICamerasManager
    {
        private readonly CameraConfig _cameraConfig;
        
        public CamerasManager(CameraConfig p_cameraConfig)
        {
            _cameraConfig = p_cameraConfig;
        }
        
        public void Initialize()
        {
            ChangeScene(1);
        }
        
        public void ChangeScene(int p_sceneIndex)
        {
            if (Camera.main == null)
            {
                return;
            }
            
            var position = _cameraConfig.GetPositionBySceneIndex(p_sceneIndex);

            if (position != null)
            {
                Camera.main.transform.position = position.Value;
            }

            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(p_sceneIndex));
        }
    }
}