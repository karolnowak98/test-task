﻿using TestTask.Cameras.Data;
using UnityEngine;
using Zenject;

namespace TestTask.Cameras.Logic
{
    public class CamerasInstaller : MonoInstaller
    {
        [SerializeField] private CameraConfig _cameraConfig;
        
        public override void InstallBindings()
        {
            Container.Bind<ICamerasManager>().To<CamerasManager>().AsSingle().WithArguments(_cameraConfig);
            Container.Bind<IInitializable>().To<ICamerasManager>().FromResolve();
        }
    }
}