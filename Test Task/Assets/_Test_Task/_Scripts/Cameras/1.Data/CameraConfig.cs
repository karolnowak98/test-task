﻿using System;
using UnityEngine;

namespace TestTask.Cameras.Data
{
    [CreateAssetMenu(fileName = "Camera config", menuName = "Configs/Camera config")]
    public class CameraConfig : ScriptableObject
    {
        [SerializeField] private CameraScene[] _cameras;
        
        public Vector3? GetPositionBySceneIndex(int p_buildSceneIndex)
        {
            foreach (var camera in _cameras)
            {
                if (camera.BuildSceneIndex == p_buildSceneIndex)
                {
                    return camera.Position;
                }
            }
            
            return null;
        }
    }

    [Serializable]
    public struct CameraScene
    {
        [SerializeField] private int _buildSceneIndex;
        [SerializeField] private Vector3 _position;

        public int BuildSceneIndex => _buildSceneIndex;
        public Vector3 Position => _position;
    }
}
